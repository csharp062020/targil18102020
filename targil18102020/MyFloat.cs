﻿using System;

namespace targil18102020
{
    class MyFloat
    {
        public int IntrgerPart { get; set; }
        public int FractionalPart { get; set; }
        public bool Positive { get; set; }

        public MyFloat(int intrgerPart, int fractionalPart, bool positive)
        {
            IntrgerPart = intrgerPart;
            FractionalPart = fractionalPart;
            Positive = positive;
        }


        public int this[int index]
        {
            get
            {
                String newString = $"{this.IntrgerPart}{this.FractionalPart}";
                try
                {
                    return Convert.ToInt32($"{newString[index-1]}");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        public static MyFloat operator +(MyFloat a, double b)
        {

            MyFloat aNew = new MyFloat(a.IntrgerPart, a.FractionalPart, a.Positive);


            double doubleNumner = 0;
            doubleNumner = Temp(aNew);
            doubleNumner += b;
            return OpAddition(doubleNumner, aNew);
        }

        public static MyFloat operator -(MyFloat a, double b)
        {
            MyFloat aNew = new MyFloat(a.IntrgerPart, a.FractionalPart, a.Positive);
            double doubleNumner = 0;
            doubleNumner = Temp(aNew);
            doubleNumner -= b;
            return OpAddition(doubleNumner, aNew);
        }


        public static MyFloat operator *(MyFloat a, double b)
        {
            MyFloat aNew = new MyFloat(a.IntrgerPart, a.FractionalPart, a.Positive);
            double doubleNumner = 0;
            doubleNumner = Temp(aNew);
            doubleNumner *= b;
            return OpAddition(doubleNumner, aNew);
        }

        public static MyFloat operator /(MyFloat a, double b)
        {
            MyFloat aNew = new MyFloat(a.IntrgerPart, a.FractionalPart, a.Positive);
            double doubleNumner = 0;
            doubleNumner = Temp(aNew);
            doubleNumner /= b;
            return OpAddition(doubleNumner, aNew);
        }

        public static bool operator ==(MyFloat a, double b)
        {
            MyFloat aNew = new MyFloat(a.IntrgerPart, a.FractionalPart, a.Positive);
            double doubleNumner = 0;
            doubleNumner = Temp(aNew);
            return doubleNumner == b;
        }

        public static bool operator >(MyFloat a, double b)
        {
            MyFloat aNew = new MyFloat(a.IntrgerPart, a.FractionalPart, a.Positive);
            double doubleNumner = 0;
            doubleNumner = Temp(aNew);
            return doubleNumner > b;
        }

        public static bool operator <(MyFloat a, double b)
        {
            MyFloat aNew = new MyFloat(a.IntrgerPart, a.FractionalPart, a.Positive);
            double doubleNumner = 0;
            doubleNumner = Temp(aNew);
            return doubleNumner < b;
        }

        public static bool operator !=(MyFloat a, double b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            return this == obj as MyFloat;
        }


        public override string ToString()
        {
            return $"{nameof(IntrgerPart)}: {IntrgerPart}, {nameof(FractionalPart)}: {FractionalPart}, {nameof(Positive)}: {Positive}";
        }

        private static MyFloat OpAddition(double doubleNumner, MyFloat aNew)
        {
            if (doubleNumner < 0)
            {
                aNew.Positive = false;
                doubleNumner *= (-1);
            }
            else
            {
                aNew.Positive = true;
            }

            aNew.IntrgerPart = (int)(doubleNumner / 1);
            aNew.FractionalPart = temp2(doubleNumner%1);
            

            return aNew;
        }

        private static int temp2(double fractionalPart)
        {
            while (((float)fractionalPart % 1) != 0)
            {
                fractionalPart *= 10;
            }

            return (int)fractionalPart;
        }

            private static double Temp(MyFloat aNew)
        {
            double temp = aNew.FractionalPart;
            while (temp > 1)
            {
                temp /= 10;
            }

            temp += (double)aNew.IntrgerPart;

            if (!aNew.Positive)
            {
                temp *= (-1);
            }

            return temp;
        }
    }
}